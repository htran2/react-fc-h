import React from 'react'
import './rtv.scss'

export default class TV extends React.PureComponent {
  render = () => {
    return (
      <div className="tv">
        <h1>TV</h1>
        <h3>{`TV POWER (value from parent)=${this.props.power}`}</h3>
      </div>
    )
  }
}