import React from 'react'
import TV from './TV'
import './rtv.scss'


export default class Remote extends React.PureComponent {
	constructor(props) {
    super(props);
    this.state = { power: false };
  }
	render = () => {
	  return (
	  	<div className="remote-control">
	  	  <h1>REMOTE CONTROLLER (Parent component)</h1>
	  	  POWER (state value in parent)
	  	  <input type="checkbox"
	  	    value={this.state.power}
	  	    onClick={() => this.setState({ power: !this.state.power })}
	  	  />
	  	  {` (${this.state.power})`}
	  	  <TV power={this.state.power} />
	  	</div>
	  )
	}
}