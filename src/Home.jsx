import { Link } from "react-router-dom"

const Home = () => {
  return (
    <>
      <h2>Functional components and some hooks</h2>    
      <div className="items">
        <Link to={`/minimum`}>Minimum</Link>
        <Link to={`/stateful`}>State</Link>
        <Link to={`/effected`}>Effect - single</Link>
        <Link to={`/effected-multi`}>Effects - multi</Link>
        <Link to={`/fetch-data`}>Fetching data as effect</Link>
        <Link to={`/own-hook`}>My own hooks</Link>
        <Link to={`/contexted`}>Context</Link>
        <Link to={`/reffed`}>Ref value</Link>
        <Link to={`/reffed-input`}>Ref a DOM element</Link>
        <Link to={`/reffed-forward`}>Ref forward</Link>
        <Link to={`/memo-hooked`}>Memo (hook)</Link>
        <Link to={`/memo-func`}>Memo (HOC)</Link>
        <Link to={`/callbacked`}>Callback</Link>
        <Link to={`/portalled`}>Portal</Link>
      </div>
    </>
  )

}

export default Home