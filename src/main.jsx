import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from './Home'
import './main.scss'
import Remote from './parent-state/Remote'
import C from './components/Minimum'
import Stateful from './components/Stateful'
import Effected from './components/Effected'
import EffectedMulti from './components/EffectedMulti'
import FetchData from './components/FetchData'
import OwnHook from './components/OwnHook'
import Contexted from './components/Contexted'
import Reffed from './components/Reffed'
import ReffedInput from './components/ReffedInput'
import ForwardedReffed from './components/ForwardedReffed'
import MemoFunc from './components/MemoFunc'
import MemoHooked from './components/MemoHooked'
import Callbacked from './components/Callbacked'
import Portalled from './components/Portalled'

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <Error />,
  },
  { path: "/parent-state", element: <Remote /> },
  { path: "/minimum", element: <C value="12345" /> },
  { path: "/stateful", element: <Stateful /> },
  { path: "/effected", element: <Effected /> },
  { path: "/effected-multi", element: <EffectedMulti /> },
  { path: "/fetch-data", element: <FetchData /> },
  { path: "/own-hook", element: <OwnHook /> },
  { path: "/contexted", element: <Contexted /> },
  { path: "/reffed", element: <Reffed /> },
  { path: "/reffed-input", element: <ReffedInput /> },
  { path: "/reffed-forward", element: <ForwardedReffed /> },
  { path: "/memo-hooked", element: <MemoHooked /> },
  { path: "/callbacked", element: <Callbacked /> },
  { path: "/memo-func", element: <MemoFunc /> },
  { path: "/portalled", element: <Portalled /> },
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)


