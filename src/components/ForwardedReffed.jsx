import { useRef, createRef, forwardRef } from 'react'


const __ForwardedReffed = () => {
  const vidRef = useRef()
  return (
    <>
      <div>{`This is HTML <video> element`}</div>
      <video width="600" ref={vidRef}>
        <source src="people-slow.mp4" type="video/mp4" />
      </video>
      <br />
      <button onClick={() => vidRef.current.pause()}>STOP</button>
      <button onClick={() => vidRef.current.play()}>PLAY</button>
    </>
  )
}



// Now with my own Video _functional_ component 

const ForwardedReffed = () => {
  const vidRef = createRef();
  return (
    <>
      <div>{`This is my own <SmallVideo> component:`}</div>
      <SmallVideo src="horse.mp4" type="video/mp4" ref={vidRef} />
      <br />
      <button onClick={() => vidRef.current.pause()}>stop</button>
      <button onClick={() => vidRef.current.play()}>play</button>
    </>
  )
}

/// CAN'T WRITE: const SmallVideo = ({src, type, ref}) => {...}
const SmallVideo = forwardRef((vidProps, ref) => (
  <video ref={ref} width="600">
    <source src={vidProps.src} type={vidProps.type} />
  </video>
))


export default ForwardedReffed




/* 
- C is functional component, not a DOM instance, no ref 
- can't write <C ref=...>
- but its gut is a DOM node, which has a ref
- i shall expose that ref
- so, "forward ref a component C"
- C' = HOC forwardRef(C's render(cprops, ref))
- In C's parent, we write <C ref={r}> and use it as r.current
- and the r is just r = useRef() or createRef() 
- r.current has ALL functionalities of the exposed instance
- if you don't like it, useImperativeHandle to customize the expose 
*/