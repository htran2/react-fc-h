import { useState } from 'react'

const Stateful = () => {
  // [identifier, setter] = keyword (initial value)
  const [ s1, changeS1 ] = useState(12345)
  const [ s2, setChicks ] = useState('🐥')
  
  return (
    <>
      <h1>Stateful component</h1>
      <p>{`States: s1=${s1}, s2=${s2}`}</p>
      <button onClick={() => changeS1(s1 + 1)}>increment s1</button>
      <button onClick={() => setChicks(s2 + s2)}>double s2</button>
    </>
  )
}

export default Stateful