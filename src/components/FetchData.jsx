/*
Vocabulary: Side-effect? Hook? 
As in "useEffect is the hook
that manages the side-effects in functional components" 
😱 🚑



OK. Let's try.

Function pure:  (inputs) ---do-something---> output, predictable 100%, unit-testable
Functional component = function(props) ----do-something---> visual component JSX
Side-effect = something outside the function's scope, maybe predictable or not, but desirable










Ex: POWER button on my flashlight: pure
Ex: POWER button on my TV: pure + side effect
Ex: I drink beer --> I get drunk, but I also become happy/sad/silly/...
Ex: I sing on a microphone ---> amplified sound, but I also want 'echo'
Good engineering: modularize concerns:
  - pure output (i.e. rendering) 
  - effects
Why? because they are independent.


How to achieve in React? use "hook"

Hook = a mechanism in component function to obtain desired effect

The "use" pseudo-keyword in React
*/


import { useState, useEffect } from 'react'
import axios from 'axios'

const FetchData = () => {
  const [ data, setData ] = useState()
  
  useEffect(() => {
    const url = 'http://wttr.in/Saigon?format=j1'
    axios.get(url).then(resp => setData(resp.data))
  }, []);
  

  return (
    <>
      <h1>Using effect to load async data</h1>
      {data ? JSON.stringify(data) : <h3>No data...</h3>}
    </>
  )
}

export default FetchData



/* So, to quote their words:
"useEffect is a React Hook that lets you synchronize a component 
with an external system."
*/


/* Conventions/rules on dependencies (reactive values in the []):
 - not supplied: always executes the effector
 - []: executes once on initial rendering
 - [props, states]: executes whenever one of these changed 
*/