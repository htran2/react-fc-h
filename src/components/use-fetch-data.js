/*
My own hook: a kinda utility function 
Rule: naming starting with "use"
*/


import { useState, useEffect } from 'react'
import axios from 'axios'

const useFetchData = (url) => {
  const [ data, setData ] = useState()
  const [ loading, setLoading ] = useState(false)
  const [ error, setError ] = useState()
  
  useEffect(() => {
    setLoading(true)
    setError(null)
    axios.get(url).then(resp => {
      setData(resp.data)
      setLoading(false)
    }).catch(err => {
      setData(null)
      setError(err.toString())
      setData(null)
      setLoading(false)
    })
  }, []);

  return { data, loading, error }
}

export default useFetchData
