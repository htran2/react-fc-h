import { useState, useEffect } from 'react'

const Effected = () => {

  const [ s, setS ] = useState(0)

  
  const effector = () => {
    console.info(`%c Effector... s=${s}`, 'color: gold; background: dodgerblue')
  
  }
  const reactives = [s]
  

  // keyword "use" Effect ( effector, [reactive values inside effector] )
  useEffect(effector, reactives);


  return (
    <>
      <h1>Component with effect</h1>
      {`State s = ${s}`}

      <button onClick={() => setS(s + 1)}>s++</button>
      

      <Child x={s*100}/>

    </>
  )
}







// <Child x={s*100}/>
const Child = (props) => {



  useEffect(() => {
    console.info('%c Effector in child', 'color: dodgerblue; background: gold')
  }, []);
  


  return <div>{`Child's props.x = ${props.x}`}</div>
}





export default Effected


/* What React does:

- render, mount DOM
- effector()
- update loop
    if (need update) re-render, re-mount DOM
    if (any dependency changed)
      effector()
  
*/

/*
Effector can return a function, if so, React executes it:
  + when component unmounts
  + right before next scheduled effector
We use it to clean up (as in componentWillUnmount)
*/



/* Life cycle
- HTML elements: span, div, h1, table, button, select,...
- Early 2010s, "web components (custom elements)" movement 
- Guidelines require lifecycle methods: 
     .constructed
     .attached to DOM
     .attribute changed
     .detached
     Example https://bignerdranch.com/blog/learn-the-lifecycle-of-a-web-component-by-building-a-custom-element/
- React's JSX
     .constructor
     .componentDidMount
     .shouldComponentUpdate
     .componentDidUpdate
     .componentWillUnmount
     v17 no more:
     componentWillMount
     componentWillReceiveProps
     componentWillUpdate
- introducing "hooks":
     useState
     useEffect
     useMemo
     useXXXXX
*/


