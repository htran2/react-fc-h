import { useState, useEffect, useCallback } from 'react'

const useToggler = (value) => {
  const [ status, setStatus ] = useState(value)
  
  const toggler = useCallback(() => setStatus(status => !status), []);

  return { status, toggler }
}

export default useToggler
