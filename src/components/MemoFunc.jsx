import { useState, memo } from 'react'

const MemoFunc = () => {
  const [ n, setN ] = useState(1)

  return (
    <> 
      <h1>Memo</h1>
      {`State values n = ${n}`}<br />
      <button onClick={() => setN(n + 1)}>Add duck</button>
      <Ducks howMany={n}/> 
      <hr />
      <FivePigs /> 
    </>
  )
}


const Ducks = ({howMany}) => {
  let n = ''
  for (let i = 0; i < howMany; i++)
    n += '🦆';
  console.info('Drawing ducks...')
  return <h1>{n}</h1>
}

const __FivePigs = () => {
  console.info('Drawing five pigs...')
  return <h1>🐖🐖🐖🐖🐖</h1>
}

const FivePigs = memo(() => {
  console.info('Drawing five pigs (memoized)...')
  return <h1>🐖🐖🐖🐖🐖</h1>
})

export default MemoFunc


/*
  - FiveDucks is "part" of this multi-part component.
  - When state changes, this component re-renders all parts.
  - If you want to avoid unnecessary re-rendering of some part that is
    not affected by state change, you can "skip" re-rendering by 
    "memoize" it  

*/ 