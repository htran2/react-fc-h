import { useState } from 'react'
import { createPortal } from 'react-dom'
import useToggler from './hook-toggler'
import useLocalStorage from './hook-local-storage'

const Notes = ({visible, close}) => {
//	const [contents, setContents] = useState('')
  const [contents, setContents] = useLocalStorage('my-notes')

  return (
    <div className={`notes ${(visible ? '' : 'hidden')}`}>
      <div style={{ float: 'right' }}>
        <button onClick={close}>✓</button>
      </div>
      <h3>Notes</h3>
      <textarea
        placeholder="Write anything here..." 
        value={contents}
        onChange={e => setContents(e.target.value)}
      />
    </div>
  )
}

const Portalled = () => {
  const { status: visible, toggler: setVisible } = useToggler(false)
  return (
    <div>
      <div style={{ float: 'right' }}>
        <button onClick={() => setVisible(true)}>🖋️</button>
      </div>
      <h1>Some app</h1>
      <h2>Stuffs</h2>
      <h2>More stuffs</h2>
      {createPortal(
        <Notes visible={visible} close={() => setVisible(false)} />,
        document.getElementById('notes'))
      }
    </div>
  )
}

export default Portalled




