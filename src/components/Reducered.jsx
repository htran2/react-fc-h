import { useState, useCallback } from 'react'

const Reducered = () => {
  const [ past, setPast ] = useState()
  const [ present, setPresent ] = useState(0)
  const [ future, setFuture ] = useState(0)

  



  return (
    <> 
      <h1>Callback</h1>
      {`State values s1 = ${s1}, s2 = ${s2}`}<br />
      <button onClick={() => setS1(s1 + 1)}>Change s1</button>
      <button onClick={() => setS2(s2 + 1)}>Change s2</button>
    </>
  )
}

export default Reducered


/*
  x = useCallback(f, [dependencies]) 

  you get one function object all the times

*/ 

// https://www.baeldung.com/cs/convert-color-hsl-rgb