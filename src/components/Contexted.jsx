/*
Component hierarchy:
  Top -> Grandma -> Mama -> Daughter -> Baby

Needs: Baby needs reactive values from Top component 

-----------------------------------------------------------
Without context:

Baby component needs to display: Price: 12.32 CAD | Preço: 49 BRL
  by relying on {props.language} {props.currency}

Daughter:
  <Baby language={props.language} currency={props.currency} />

Mama:
  <Daughter language={props.language} currency={props.currency} />

Grandma:
  <Mama language={props.language} currency={props.currency} />

Top:
  <Grandma language=... currency=... />

*/



/* How about a "wifi" solution?
*/



import { useState, createContext, useContext } from 'react'

const Wifi = createContext(null)

const Contexted = () => {
  const [ language, setLanguage ] = useState('english')
  const [ currency, setCurrency ] = useState('CAD')
  return (
    <Wifi.Provider value={{ language, currency }}>
      <div style={{ background: 'peru', marginBottom: '10rem' }}>
        <button onClick={() => { setLanguage('english'); setCurrency('CAD') }}>Canada</button>
        <button onClick={() => { setLanguage('portugese'); setCurrency('BRL') }}>Brasil</button>
      </div>
      <Grandma />
    </Wifi.Provider>
  )
}

const Grandma = () => <Mama />
const Mama = () => <Daughter />
const Daughter = () => <Baby />
const Baby = () => {
  return (
    <div>
      Baby component, knowing {JSON.stringify(useContext(Wifi))}
    </div>
  )
}

export default Contexted