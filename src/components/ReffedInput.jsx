/* Typical example of ref: access DOM element directly
*/


import { useRef } from 'react'

const ReffedInput = () => {
  const theSecondInput = useRef();
  return (
    <>
      <input />
      <input ref={theSecondInput} value="12345 67890" onChange={() => null} />
      <input />
      <br />
      <button onClick={() => {
        theSecondInput.current.select()
        theSecondInput.current.focus()
      }}>
        Focus and select the second input
      </button>
      

      <div style={{ height: '150rem' }} />
      
      <button onClick={() => {
        theSecondInput.current.scrollIntoView()
        theSecondInput.current.select()
        theSecondInput.current.focus()
      }}>
        Scroll up to the second input
      </button>
    </>
  )
}

export default ReffedInput


/* 
(https://deepscan.io/docs/rules/react-func-component-invalid-ref-prop)
Your functional component cannot have a prop called "ref" 
since there is no instance to refer to
*/