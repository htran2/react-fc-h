/* A ref value:
 1- mutable
 2- alive thru all re-renders
 3- its change does NOT trigger re-render
 4- no getter/setter, just access its ".current" field directly

Compared to a state value: 1-like, 2-like, 3-unlike, 4-unlike
*/


import { useRef, useState } from 'react'

const Reffed = () => {
  const [ s, setS ] = useState(100)
  const r = useRef(1000)
  let x = 1
  return (
    <>
      <h1>{`Ref`}</h1>
      {`State value s = ${s}`}<br />
      {`Local value x = ${x}`}<br />
      {`Ref value x = ${r.current}`} 👈 No re-render when ref changes<br />
      <button onClick={() => setS(s + 1)}>Change state value</button>
      <button onClick={() => x += 1}>Change local value</button>
      <button onClick={() => r.current += 1}>Change ref value</button>
    </>
  )
}

export default Reffed