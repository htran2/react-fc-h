import { useState, useMemo } from 'react'

const MemoHooked = () => {
  const [ s1, setS1 ] = useState(0)
  const [ s2, setS2 ] = useState(0)
  //const expensive = slowComputation()
  const expensive = useMemo(slowComputation, [s1]);

  return (
    <> 
      <h1>Memo hook</h1>
      {`State values s1 = ${s1}, s2 = ${s2}`}  👈 Unwanted delay <br />
      <button onClick={() => setS1(s1 + 1)}>Change s1</button>
      <button onClick={() => setS2(s2 + 1)}>Change s2</button>
      <div style={{ transform: `rotate(${s1*10}deg)`, fontSize: '10rem', width: '30rem', margin: '5rem auto' }}>
        {expensive}
      </div>  
      <div>(duck and chicken react only to s1)</div>  
    </>
  )
}


const slowComputation = () => {
  for (let i = 0, n = 0; i < 1000000000; i++) {
    n += 1;
  }
  console.info('Computing...')
  return '🦆🐓';
}

export default MemoHooked


/*
  x = useMemo(expensive_value, [dependencies])

IS LIKE:

  if (any dependency changed)
    x = expensive_value recalculated
  else
    x = expensive_value cached
*/ 