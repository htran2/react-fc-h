/*
Using my own hooks
*/


import useFetchData from './use-fetch-data'
import useToggler from './hook-toggler'

const OwnHook = () => {
  const {
    data: universitiesData,
    loading: universitiesLoading,
    error: universitiesError
  } = useFetchData('http://universities.hipolabs.com/search?country=Canada')
  const {
    data: weatherData,
    loading: weatherLoading,
    error: weatherError
  } = useFetchData('http://wttr.in/Warsaw?format=j1')
  const moodSwing = useToggler(true)

  return (
    <>
      <h1>Using my own hook to load async data</h1>
      {universitiesError && <h3>{`Error fetching universities data: ${universitiesError}`}</h3>}
      {universitiesLoading && <h2>Loading universities in Canada...</h2>}
      {universitiesData && <p style={{ color: 'olive' }}>{JSON.stringify(universitiesData)}</p>}
      <hr />
      {weatherError && <h3>{`Error fetching universities data: ${weatherError}`}</h3>}
      {weatherLoading && <h2>Loading weather in Warsaw...</h2>}
      {weatherData && <p style={{ color: 'steelblue' }}>{JSON.stringify(weatherData)}</p>}
      <hr />
      <h1>Using another own hook to flip boolean values</h1>
      <button onClick={moodSwing.toggler}>Change mood</button>
      <span style={{ fontSize: '7rem'}}>{moodSwing.status ? '☺️' : '😤'}</span>
    </>
  )
}

export default OwnHook



/* 
Custom hooks:
- let you rip out PARTS of component's functionalities (the C in MVC)
- PARTS are unit tested, simplifying component's unit testing


* need good names for PARTS to make code show clear intentions

https://kyleshevlin.com/use-encapsulation


*/
