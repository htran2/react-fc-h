import { useState, useCallback } from 'react'

const Callbacked = () => {
  const [ s1, setS1 ] = useState(0)
  const [ s2, setS2 ] = useState(0)

  // many functions, happen to be here inside the component's function
  const f1 = () => 'dog'
  const f2 = () => 'cat'
  const f3 = (x) => x + x
  const f4 = (a, b, c) => 'd e f g'

  const f200 = () => s1 + s2 + f1() + f3('🌼')



  return (
    <> 
      <h1>Callback</h1>
      {`State values s1 = ${s1}, s2 = ${s2}`}<br />
      <button onClick={() => setS1(s1 + 1)}>Change s1</button>
      <button onClick={() => setS2(s2 + 1)}>Change s2</button>
      
    </>
  )
}

export default Callbacked


/*
  ff = useCallback(f, [dependencies]) 

  you get one function object ff in all renderings

*/ 
