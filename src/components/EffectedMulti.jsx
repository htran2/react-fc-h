import { useState, useEffect } from 'react'

const EffectedMulti = () => {

  const [ s1, setS1 ] = useState(0)
  const [ s2, setS2 ] = useState(0)

  
  // React will execute these in written order, but async 
  useEffect(() => console.log(`Effector A... s1=${s1} s2=${s2}`), [s1]);
  useEffect(() => console.log(`Effector B... s1=${s1} s2=${s2}`), [s2]);
  useEffect(() => console.log(`Effector C... s1=${s1} s2=${s2}`), [s1, s2]);





  return (
    <>
      <h1>Component with multi-effects</h1>
      {`States s1=${s1} s2=${s2}`}

      <button onClick={() => setS1(s1 + 1)}>s1++</button>
      <button onClick={() => setS2(s2 - 1)}>s2--</button>
    </>
  )
}

export default EffectedMulti
