/* Class/Functional component

import React from 'react'
class C extends React.Component {
  
  render() {
    return {this.props.value}
  }
}
------------------------------------------
function C(props) {
  return {props.value}
}

*/


const C = ({ value }) => {
  return value
}



export default C