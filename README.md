# A little demo of React functional components and hooks

Code for ATB knowledge transfer session.


## Set up

```
cd somewhere

git clone https://gitlab.com/htran2/react-fc-h.git

cd r18

npm install

npm start

```

Visit `http://127.0.0.1:5173/`


## Trying
Click around, look at the URL, then see routering in `main.js`
will tell you which component it is. Then read its code.
Change code and see the intentions.
